package com.cobiscorp.cobis.test_selenium_demo;

import java.io.File;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.*;
import org.apache.commons.io.FileUtils;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeachCustomerTest {

	private static final String ID_CLIENTE = "GORF820218MDFNDR04";
	private static final String USER = "admuser";
	private static final String PASS = "admuser";
	private static final String PASSWORD = "password";
	private static final String LOGIN = "login";
	private static final String END_POINT = "http://192.168.66.93:8082/CWC/services/cobis/web/bank/views/commons/login.html";
	private static final String CHROME_CHROMEDRIVER = "./src/test/resources/chromeDriver/chromedriver.exe";
	private static final String WEBDRIVER_CHROME_DRIVER = "webdriver.chrome.driver";
	private static final String ROL = "MENU POR PROCESOS";
	private static final String OFICINA_MATRIZ = "OFICINA MATRIZ";
	private static final String TE_CREEMOS = "TE CREEMOS";
	
	private WebDriver chromeDriver;
	 

	@Before
	public void setUp() {
		System.setProperty(WEBDRIVER_CHROME_DRIVER, CHROME_CHROMEDRIVER);
		chromeDriver = new ChromeDriver();
		chromeDriver.manage().window().maximize();
		chromeDriver.get(END_POINT);

	}

	@Test
	public void testLogin() throws Exception {

		TakesScreenshot scrShot = ((TakesScreenshot) chromeDriver);

		chromeDriver.findElement(By.id(LOGIN)).sendKeys(USER);
		chromeDriver.findElement(By.id(PASSWORD)).sendKeys(PASS);
		chromeDriver.findElement(By.xpath("//button[@class='btn btn-primary btn-lg btn-block ng-scope']")).click();
		Thread.sleep(8000);
		//revisar no me funca
		chromeDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		
		
		
		chromeDriver.findElement(By.name("rol_input")).sendKeys(ROL);
		chromeDriver.findElement(By.name("office_input")).sendKeys(OFICINA_MATRIZ);
		chromeDriver.findElement(By.name("filial_input")).sendKeys(TE_CREEMOS);
		
		
		
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		takeSnapShot(chromeDriver, "C:\\temp\\Selenium\\test1.png");
		
		Thread.sleep(2000);
		
		chromeDriver.findElement(By.xpath("//button[text()='Ingresar']")).click();

		Thread.sleep(20000);
		//chromeDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		chromeDriver.findElement(By.xpath("//a[contains(@title, 'Menú')]")).click();

		Thread.sleep(8000);

		chromeDriver.findElement(By.cssSelector(".list-group-item:nth-last-child(10)")).click();
		Thread.sleep(4000);
		chromeDriver.findElement(By.cssSelector(".list-group-item:nth-last-child(3)")).click();
		//Thread.sleep(2000);
		//chromeDriver.findElement(By.cssSelector(".list-group-item:nth-last-child(2)")).click();
		
		Thread.sleep(4000);
		
		SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		takeSnapShot(chromeDriver, "C:\\temp\\Selenium\\test2.png");

		chromeDriver.switchTo().frame(1);
		
		
		Thread.sleep(5000);
		chromeDriver.findElement(By.xpath("//fieldset[@id='organizer']/div/div/div[2]/div/div/div/input")).click();
		chromeDriver.findElement(By.xpath("//fieldset[@id='organizer']/div/div/div[2]/div/div/div/input")).sendKeys(ID_CLIENTE);;
		
		//Eventos de la grila
		Thread.sleep(3000);
		chromeDriver.findElement(By.xpath("//fieldset[@id='organizer']/div/div/div[2]/div/div/div[2]/button")).click();
		Thread.sleep(1000);
		chromeDriver.findElement(By.xpath("//div[@id='gridNaturalId']/div[3]/table/tbody/tr/td[4]/span")).click();
		Thread.sleep(1000);
		chromeDriver.findElement(By.xpath("//fieldset[@id='organizer']/div/div/div[3]/div/div[4]/button[3]")).click();
		
		
		assertEquals("Success", "Success");
		
		SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		takeSnapShot(chromeDriver, "C:\\temp\\Selenium\\test4.png");

		Thread.sleep(5000);

		chromeDriver.switchTo().parentFrame();
		chromeDriver.findElement(By.cssSelector(".fa-power-off")).click();
		chromeDriver.findElement(By.xpath("//div[@id='nav-logout']/div/ul/li[2]/a/label")).click();

		
		//chromeDriver.findElement(By.cssSelector("css=#btnAuthorization"))
	}

	@After
	public void chromeDown() {
		chromeDriver.quit();
	}

	public static void takeSnapShot(WebDriver webdriver, String fileWithPath) throws Exception {
		TakesScreenshot scrShot = ((TakesScreenshot) webdriver);
		File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
		File DestFile = new File(fileWithPath);
		FileUtils.copyFile(SrcFile, DestFile);
	}

}
